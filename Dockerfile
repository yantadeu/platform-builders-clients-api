FROM maven:3.6.0-alpine AS build  
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app  
RUN mvn -f /usr/src/app/pom.xml clean package

FROM gcr.io/distroless/java  
COPY --from=build /usr/src/app/target/platform-builders-clients-api*.jar /usr/app/platform-builders-clients-api.jar  
EXPOSE 8080  
ENTRYPOINT ["java","-jar","/usr/app/platform-builders-clients-api.jar"] 