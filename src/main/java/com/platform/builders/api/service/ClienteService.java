package com.platform.builders.api.service;

import com.platform.builders.api.dto.ClienteDto;
import com.platform.builders.api.dto.ClienteResponseDto;
import com.platform.builders.api.mapper.ClienteMapper;
import com.platform.builders.api.mapper.ClienteResponseMapper;
import com.platform.builders.api.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.platform.builders.api.repository.ClienteRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public ClienteResponseDto create(ClienteDto clienteDto) {
        Cliente cliente = ClienteMapper.toClient(clienteDto);
        return ClienteResponseMapper.toDto(clienteRepository.save(cliente));
    }

    public Page<List<Map<String, String>>> find(Pageable pageable) {
        return clienteRepository.find(pageable);
    }

    public Page<ClienteResponseDto> list(Pageable pageable) {
        Page<Cliente> pageCliente = clienteRepository.findAll(pageable);
        return new PageImpl<ClienteResponseDto>(ClienteResponseMapper.convertList(pageCliente.getContent()), pageable,
                pageCliente.getTotalElements());
    }

    public Page<ClienteResponseDto> getClientByCpf(String cpf, Pageable pageable) {
        Page<Cliente> cliente = clienteRepository.findByCpf(cpf, pageable);
        return new PageImpl<>(ClienteResponseMapper.convertList(cliente.getContent()), pageable,
                cliente.getTotalElements());
    }

    public Optional<Cliente> getClientByCpf(String cpf) {
        return clienteRepository.findByCpf(cpf);
    }

    public Page<ClienteResponseDto> getClientByName(String nome, Pageable pageable) {
        Page<Cliente> cliente = clienteRepository.findByNomeContainingIgnoreCase(nome, pageable);
        return new PageImpl<ClienteResponseDto>(ClienteResponseMapper.convertList(cliente.getContent()), pageable,
                cliente.getTotalElements());
    }

    public void removerCliente(Cliente cliente) {
        clienteRepository.delete(cliente);
    }

    public Cliente update(Cliente cliente) throws Exception {
        Optional<Cliente> cli = clienteRepository.findByCpf(cliente.getCpf());
        if (cli.isPresent())
            throw new Exception("CPF exists");
        return clienteRepository.save(cliente);
    }

    public Cliente updateName(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Optional<Cliente> getClientById(Long id) {
        return clienteRepository.findById(id);
    }

    public ClienteResponseDto getClientDtoById(Long id) {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        if (!cliente.isPresent())
            return null;
        Cliente cliOps = new Cliente(cliente.get().getId(), cliente.get().getNome(), cliente.get().getCpf(),
                cliente.get().getDataNascimento());
        return ClienteResponseMapper.toDto(cliOps);
    }
}
