package com.platform.builders.api.mapper;

import com.platform.builders.api.dto.ClienteDto;
import com.platform.builders.api.models.Cliente;

public class ClienteMapper {

    public static Cliente toClient(ClienteDto clienteDto) {
        return new Cliente(clienteDto.getNome(), clienteDto.getCpf(), clienteDto.getDataNascimento());
    }
}
