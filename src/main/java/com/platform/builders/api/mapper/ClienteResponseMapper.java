package com.platform.builders.api.mapper;

import com.platform.builders.api.dto.ClienteResponseDto;
import com.platform.builders.api.models.Cliente;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClienteResponseMapper {

    public static ClienteResponseDto toDto(Cliente cliente) {
        return new ClienteResponseDto(cliente.getId(), cliente.getNome(), calculateAge(cliente.getDataNascimento()));
    }

    public static ClienteResponseDto toDto(Optional<Cliente> cliente) {
        return new ClienteResponseDto(cliente.get().getId(), cliente.get().getNome(),
                calculateAge(cliente.get().getDataNascimento()));
    }

    public static List<ClienteResponseDto> convertList(List<Cliente> clientes) {
        List<ClienteResponseDto> dtoList = new ArrayList<ClienteResponseDto>();
        for (Cliente iterator : clientes) {
            dtoList.add(toDto(iterator));
        }
        return dtoList;
    }

    public static Integer calculateAge(LocalDate dataNascimento) {
        if (dataNascimento == null)
            return 0;

        LocalDate data = LocalDate.now();
        Integer idade = data.getYear() - dataNascimento.getYear();
        if (dataNascimento.getMonthValue() > data.getMonthValue()) {
            idade -= 1;
        } else {
            if (dataNascimento.getMonthValue() == data.getMonthValue()
                    && dataNascimento.getDayOfMonth() > data.getDayOfMonth()) {
                idade -= 1;
            }
        }
        return idade;
    }

}
