package com.platform.builders.api.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.Map;

public abstract class BaseController<T, V, Z> {

    protected abstract ResponseEntity<T> create(@RequestBody @Valid V dto);

    protected abstract ResponseEntity<Page<List<Map<String, String>>>> list(Pageable pageable);

    protected abstract ResponseEntity<Page<T>> search(Pageable pageable,
            @RequestParam(required = false, defaultValue = "") String q1,
            @RequestParam(required = false, defaultValue = "") String q2);

    protected abstract ResponseEntity<T> get(Pageable pageable, @PathVariable(value = "id") Long id);

    protected abstract ResponseEntity<Z> put(@RequestBody @Valid V clienteDto, @PathVariable(value = "id") Long id);

    protected abstract ResponseEntity<Z> patch(@RequestBody String str, @PathVariable(value = "id") Long id);

    protected abstract ResponseEntity<Void> delete(@PathVariable(value = "id") Long id);

}
