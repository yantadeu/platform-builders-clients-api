package com.platform.builders.api.controller;

import com.platform.builders.api.dto.ClienteDto;
import com.platform.builders.api.dto.ClienteResponseDto;
import com.platform.builders.api.models.Cliente;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.platform.builders.api.service.ClienteService;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/clients")
@Api(value = "Clients Rest Api with Spring Boot")
@CrossOrigin(origins = "*")
public class ClienteController extends BaseController<ClienteResponseDto, ClienteDto, Cliente> {

    @Autowired
    private ClienteService clienteService;

    @Override
    @PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ApiOperation(value = "Add new client")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Client created successfully"),
            @ApiResponse(code = 409, message = "CPF Exists"),
            @ApiResponse(code = 400, message = "Invalid parameters") })
    protected ResponseEntity<ClienteResponseDto> create(@RequestBody @Valid ClienteDto clienteDto) {
        Optional<Cliente> cliente = clienteService.getClientByCpf(clienteDto.getCpf());
        if (!cliente.isPresent()) {
            return new ResponseEntity<>(clienteService.create(clienteDto), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping(path = "/", produces = { MediaType.APPLICATION_JSON_VALUE })
    @ApiOperation(value = "Get list of clients")
    @ApiResponses(value = @ApiResponse(code = 200, message = "Clients found successfully"))
    protected ResponseEntity<Page<List<Map<String, String>>>> list(Pageable pageable) {
        return new ResponseEntity<>(clienteService.find(pageable), HttpStatus.OK);
    }

    @GetMapping(path = "/search", produces = { MediaType.APPLICATION_JSON_VALUE })
    @ApiOperation(value = "List all Clients or search through the past QueryString")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Clients found successfully"),
            @ApiResponse(code = 302, message = "Clients found with parameter passed"),
            @ApiResponse(code = 404, message = "Clients not found with parameter passed") })
    protected ResponseEntity<Page<ClienteResponseDto>> search(Pageable pageable,
            @RequestParam(required = false, defaultValue = "") String nome,
            @RequestParam(required = false, defaultValue = "") String cpf) {
        if (nome.isEmpty() && cpf.isEmpty()) {
            return new ResponseEntity<>(clienteService.list(pageable), HttpStatus.OK);
        } else {
            if (nome.isEmpty()) {
                try {
                    return new ResponseEntity<>(clienteService.getClientByCpf(cpf, pageable), HttpStatus.FOUND);
                } catch (Exception e) {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } else {
                try {
                    return new ResponseEntity<>(clienteService.getClientByName(nome, pageable), HttpStatus.FOUND);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            }
        }

    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get a client by id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Client found successfully"),
            @ApiResponse(code = 404, message = "Client not found") })
    protected ResponseEntity<ClienteResponseDto> get(Pageable pageable, @PathVariable(value = "id") Long id) {
        try {
            ClienteResponseDto cliente = clienteService.getClientDtoById(id);
            if (cliente == null)
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(cliente, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update a whole client")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Updated Client Information"),
            @ApiResponse(code = 404, message = "Client not found, cannot be updated") })
    protected ResponseEntity<Cliente> put(@RequestBody @Valid ClienteDto clienteDto,
            @PathVariable(value = "id") Long id) {
        try {
            Optional<Cliente> cliOps = clienteService.getClientById(id);
            if (!cliOps.isPresent())
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);

            Cliente cliente = new Cliente(cliOps.get().getId(), clienteDto.getNome(), clienteDto.getCpf(),
                    clienteDto.getDataNascimento());
            return new ResponseEntity<>(clienteService.update(cliente), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

    @PatchMapping(path = "/{id}", consumes = { MediaType.ALL_VALUE })
    @ApiOperation(value = "Update name of client")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Updated Client Information"),
            @ApiResponse(code = 404, message = "Client not found, cannot be updated") })
    protected ResponseEntity<Cliente> patch(@RequestBody String nome, @PathVariable(value = "id") Long id) {
        try {
            Optional<Cliente> cliOps = clienteService.getClientById(id);
            if (!cliOps.isPresent())
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            Cliente cliente = new Cliente(cliOps.get().getId(), nome.intern(), cliOps.get().getCpf(),
                    cliOps.get().getDataNascimento());
            return new ResponseEntity<>(clienteService.updateName(cliente), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete a client by id")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Deleted client successfully"),
            @ApiResponse(code = 404, message = "Client not found, cannot be deleted") })
    protected ResponseEntity<Void> delete(@PathVariable(value = "id") Long id) {
        try {
            Optional<Cliente> cliente = clienteService.getClientById(id);
            if (!cliente.isPresent())
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            Cliente cliOps = new Cliente(cliente.get().getId(), cliente.get().getNome(), cliente.get().getCpf(),
                    cliente.get().getDataNascimento());
            clienteService.removerCliente(cliOps);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}
