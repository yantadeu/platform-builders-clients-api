# Users API With Spring Boot

## The Problem

Desenvolva um projeto Java com Spring Boot (utilizando qualquer modulo que achar necessário), que possua uma api REST com CRUD de User (id, nome, cpf, dataNascimento). O CRUD deve possuir uma api de GET, POST, DELETE, PATCH e PUT.

A api de GET deve aceitar query strings para pesquisar os users por CPF e nome. Também é necessário que nessa api os users voltem paginados e que possua um campo por user com a idade calculado dele considerando a data de nascimento.

O banco de dados deve estar em uma imagem Docker ou em um sandbox SAAS (banco SQL).

O projeto deve estar em um repositório no BitBucket e na raiz do projeto deve conter um Postman para apreciação da api.

## Technologies Used Technologies

- Java
- Maven
- Spring Boot
- PostgreSQL
- Spring Data JPA
- Lombok
- Docker
- Swagger

## Dependencies to execute project

- Maven
- Docker
- Docker Compose

## How to Execute

Run the commands at the root of the project:

1. `docker-compose up --build`

## Swagger

Running locally, access by address ***<http://localhost:8080/swagger-ui.html>***

## Postman colletion

Import file in postman: [users_rest_api_with_spring_boot.postman_collection.json](users_rest_api_with_spring_boot.postman_collection.json)

## Future improvements

1. Build automated unit and integration tests, at this stage of the API no tests were implemented;
2. Implement yaml file for ci/cd (for the chosen tool);
3. Implement yaml file for kubernetes management;
4. Docker composer read environment variables from .env file;
5. Map the volume of postgres in docker-compose.yaml;
